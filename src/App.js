import { useState } from "react";
import logo from "./logo.svg";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./App.css";
import "./index.css";

function App() {
  const [count, setCount] = useState(0);
  const [string, setString] = useState("???");

  return (
    <div className="App">
      <header className="App-header">
        <div className="row">
          <div className="col-12">
            <div>
              <h4>Total Click</h4>
              <h1>{count}</h1>
              <button 
                className="btn dark text-success btn-light mx-3 App-button"
                onClick={() => setCount(count - 1)}
              >
                -
              </button>
              <button
                className="btn dark text-warning btn-light mx-3 App-button"
                onClick={() => setCount(0)}
              >
                Reset
              </button>
              <button
                className="btn dark text-info btn-light mx-3 App-button"
                onClick={() => setCount(count + 1)}
              >
               +
              </button>
            </div>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col-12">
            <div>
              <h4>Kamu pilih yang mana !!!</h4>
              <h2 className="p-1">Kamu memilih, {string}</h2>
              <button
                className="btn btn dark text-success btn-light mx-3 App-button"
                onClick={() => setString("Kalkulus Yang Samgatlah Muda")}
              >
                Kalkulus 
              </button>
              <button
                className="btn btn dark text-warning btn-light mx-3 App-button"
                onClick={() => setString("???")}
              >
                Reset
              </button>
              <button
                className="btn btn dark text-info btn-light mx-3 App-button"
                onClick={() => setString(" Sejarah Yang Samgatlah Muda")}
              >
               Sejarah 
              </button>
            </div>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
